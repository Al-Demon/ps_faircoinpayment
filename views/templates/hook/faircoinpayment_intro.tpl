{*
* 2018 Al-Demon for KOMUN based in PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Al-Demon <al-demon@3toques.es>
*  @copyright  2018 Al-Demon for KOMUN based in PrestaShop
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA and KOMUN is free like the wind
*}

<section>
<div class="container">
 <div class="row">
 <div class="col-sm-6">
	<img src="../modules/faircoinpayment/faircoin.png" alt="{l s='Pay with faircoin' mod='faircoinpayment'}" width="133" height="200"/>
	<p>{l s='Faircoin Address:' mod='faircoinpayment'} {$faircoinAddress nofilter}</p>
 </div>
 <div class="col-sm-6">
	<img src="https://api.qrserver.com/v1/create-qr-code/?data={$faircoinAddress nofilter}&size=200x200">
 </div>
 </div>
<br>
  <p>
    {l s='Please transfer the invoice amount to our fair account. You will receive our order confirmation by email containing fair details and order number.' mod='faircoinpayment'}
<br>
    {l s='Goods will be reserved %s days for you and we\'ll process the order immediately after receiving the payment.' sprintf=[$faircoinReservationDays] mod='faircoinpayment'}

    {if $faircoinCustomText }
        <a data-toggle="modal" data-target="#faircoin-modal">{l s='More information' mod='faircoinpayment'}</a>
    {/if}
  </p>
</div>
  <div class="modal fade" id="faircoin-modal" tabindex="-1" role="dialog" aria-labelledby="Faircoin information" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h2>{l s='Faircoin' mod='faircoinpayment'}</h2>
        </div>
        <div class="modal-body">
          <p>{l s='Payment is made by transfer of the invoice amount to the following account:' mod='faircoinpayment'}</p>
          {include file='module:faircoinpayment/views/templates/hook/_partials/payment_infos.tpl'}
          {$faircoinCustomText nofilter}
        </div>
      </div>
    </div>
  </div>
</section>
